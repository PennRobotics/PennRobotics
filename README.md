This is a personal GitLab account and not affiliated with the University of Pennsylvania, although I did graduate from Penn (ROBO &rsquo;17).

My current career focus is software for hardware debuggers.
  
<br/>

<div id="skills">
  <a href="https://www.python.org/"><img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/python/python-original.svg" title="Python" alt="Python" width="40" height="40"/></a>&nbsp;
  <a href="https://gcc.gnu.org/"><img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/c/c-plain.svg" title="C" alt="C" width="40" height="40"/></a>&nbsp;
  <a href="https://gcc.gnu.org/"><img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/cplusplus/cplusplus-plain.svg" title="C++" alt="C++" width="40" height="40"/></a>&nbsp;
  <a href="https://www.mathworks.com/"><img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/matlab/matlab-original.svg" title="Matlab" alt="Matlab" width="40" height="40"/></a>&nbsp;
  <a href="https://www.solidworks.com/"><img src="https://gitlab.com/PennRobotics/PennRobotics/raw/main/dassault-logo.svg" title="SOLIDWORKS" alt="SOLIDWORKS" width="40" height="40"/></a>&nbsp;
  <a href="https://www.kicad.org/"><img src="https://gitlab.com/PennRobotics/PennRobotics/raw/main/kicad-logo.svg" title="KiCad" alt="KiCad" width="40" height="40"/></a>&nbsp;
  <a href="https://www.lauterbach.com/"><img src="https://gitlab.com/PennRobotics/PennRobotics/raw/main/lb-logo-inv.svg" title="Lauterbach" alt="Lauterbach" width="40" height="40"/></a>&nbsp;
</div>

<br/>

<div id="badges">
  <a href="https://www.linkedin.com/in/pennrobotics/"><img src="https://img.shields.io/badge/-steelblue?style=for-the-badge&logo=logmein&logoColor=white" alt="LogMeIn Badge"/></a>
  <a href="https://scholar.google.com/citations?user=BsiD-pkAAAAJ"><img src="https://img.shields.io/badge/-cornflowerblue?style=for-the-badge&logo=googlescholar&logoColor=white" alt="Google Scholar Badge"/></a>
  <a href="https://www.youtube.com/@penn_robotics"><img src="https://img.shields.io/badge/-crimson?style=for-the-badge&logo=youtube&logoColor=white" alt="YouTube Badge"/></a>
  <a href="https://gitlab.com/PennRobotics/"><img src="https://img.shields.io/badge/-tomato?style=for-the-badge&logo=gitlab&logoColor=white" alt="GitLab Badge"/></a>
  <a href="https://news.ycombinator.com/user?id=PennRobotics"><img src="https://img.shields.io/badge/-orange?style=for-the-badge&logo=ycombinator&logoColor=white" alt="Hacker News Badge"/></a>
  <a href="https://stackoverflow.com/users/10641561"><img src="https://img.shields.io/badge/-darkorange?style=for-the-badge&logo=stackoverflow&logoColor=white" alt="Stack Overflow Badge"/></a>
</div>

#### TODO

- [ ] Finish moving the reel books into **/public/**
- [ ] Release more source as _public_
- [ ] Continue stalled projects
- [ ] Fix the entire domain (including subdomain redirects to project-specific Gitlab Pages)
- [ ] Move totally off AWS
- [ ] Publish more across domains
- [ ] Revise/restrict repo features (issues, PRs, milestones, CI/CD)
